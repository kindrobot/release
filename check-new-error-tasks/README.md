# README for mediawiki_new_errors_checker.py

This script keeps the `mediawiki-new-errors` dashboard clean on logstash by:

1. Fetching the json describing the existing dashboard from OpenSearch
2. Finding any dashboard filters that contain Phab-style task IDs (matching /T\d+/)
3. Searching Phabricator for all task IDs
4. Creating a new json dashboard, removing task filters from the dashboard
   unless their status is "open"
5. Unless this is a `--dry-run`, re-writing the old dashboard with the new


## Usage

    mediawiki_new_errors_checker.py [-h] [-v] [-i ID] [-d]

    options:
      -h, --help      show this help message and exit
      -v, --verbose   Enable verbose output
      -i ID, --id ID  ID of Dashboard (default: 0a9ecdc0-b6dc-11e8-9d8f-dbc23b470465)
      -d, --dry-run   Don't delete, just do a dry run

## Setup

The dependencies to run the script are:

1. Disqus's [phabricator][] python module: `pip install phabricator`
2. Python [requests][]: `pip install requests`
3. A valid ldap username and password that can access https://logstash.wikimedia.org
4. A phabricator API token, usually stored in `~/.arcrc` and formatted as:

    {
        "config": {
            "default": "https://phabricator.wikimedia.org/"
        },
        "hosts": {
            "https://phabricator.wikimedia.org/api/": {
                "token": "api-token_which_starts_with_api"
            }
        }
    }

[phabricator]: <https://pypi.org/project/phabricator/>
[requests]: <https://pypi.org/project/requests/>

## Tests

Use pytest to run tests, or use `tox` from the top-level directory.
